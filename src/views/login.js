import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";
import app from "../base.js";
import { AuthContext } from "../services/auth";

import { makeStyles, withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import AccountCircleOutlinedIcon from "@material-ui/icons/AccountCircleOutlined";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Paper from "@material-ui/core/Paper";
import { Button } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  paper: {
    padding: theme.spacing(4),
    marginTop: theme.spacing(10),
    borderRadius: 8
  },
  marginDistance: {
    marginBottom: theme.spacing(3)
  },
  gradientButton: {
    background: "linear-gradient(90deg, #00fff4 0%, #ff00fe 100%)",
    borderRadius: 25,
    color: "white",
    fontWeight: "bold",
    height: 48,
    width: 300,
    marginTop: 20,
    "&:hover": {
      background: "linear-gradient(90deg, #00fff4 10%, #ff00fe 120%)",
      boxShadow: "0 5px 5px 2px rgba(200, 105, 135, .4)"
    }
  }
}));

const LinkButton = withStyles(() => ({
  root: {
    textTransform: "none"
  }
}))(Button);

const Login = ({ history }) => {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    userName: "",
    password: "",
    showPassword: false,
    error: false,
    recovery: false
  });

  const onLogin = useCallback(
    async (email, password) => {
      try {
        await app.auth().signInWithEmailAndPassword(email, password);
        history.push("/");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/" />;
  }

  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleShowOptions = prop => () => {
    setValues({ ...values, [prop]: !values[prop] });
  };

  const onRecovery = prop => () => {
    /* Simula opcion de recuperación de contraseña */
    alert(
      "Se envió un enlace para restablecer la contraseña al correo: " + prop
    );
  };

  return (
    <Grid container direction="column" justify="center" alignItems="center">
      <Paper className={classes.paper} elevation={20}>
        {values.recovery ? (
          <>
            <Typography align="center" variant={"h4"}>
              Recuperar
            </Typography>
            <Typography align="center" paragraph variant={"h4"}>
              contraseña
            </Typography>
          </>
        ) : (
          <Typography align="center" paragraph variant={"h4"}>
            Login
          </Typography>
        )}
        <Grid item xs={12} sm={12} className={classes.marginDistance}>
          <FormControl fullWidth error={values.error}>
            <Typography align="left">Usuario</Typography>
            <Input
              id="userName"
              type="text"
              value={values.userName}
              placeholder={
                values.recovery
                  ? "Escribe tu correo electrónico"
                  : "Escribe tu nombre de usuario"
              }
              onChange={handleChange("userName")}
              startAdornment={
                <InputAdornment>
                  <IconButton aria-label="lock">
                    <AccountCircleOutlinedIcon />
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
        </Grid>

        {!values.recovery && (
          <Grid item xs={12} lg={12} className={classes.marginDistance}>
            <FormControl fullWidth error={values.error}>
              <Typography align="left">Contraseña</Typography>
              <Input
                id="pass"
                type={values.showPassword ? "text" : "password"}
                value={values.password}
                placeholder="Escribe tu contraseña"
                onChange={handleChange("password")}
                startAdornment={
                  <InputAdornment>
                    <IconButton aria-label="lock">
                      <LockOutlinedIcon />
                    </IconButton>
                  </InputAdornment>
                }
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleShowOptions("showPassword")}
                    >
                      {values.showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
          </Grid>
        )}

        <Grid item className={classes.marginDistance}>
          <Button
            className={classes.gradientButton}
            onClick={
              !values.recovery
                ? () => onLogin(values.userName, values.password)
                : onRecovery(values.userName)
            }
          >
            {values.recovery ? "Enviar" : "Entrar"}
          </Button>
        </Grid>

        <Grid container justify="flex-end">
          <LinkButton
            color="primary"
            className={classes.forgot}
            onClick={handleShowOptions("recovery")}
          >
            {values.recovery ? "Regresar a Login" : "Olvidé mi contraseña"}
          </LinkButton>
        </Grid>
      </Paper>
    </Grid>
  );
};

export default withRouter(Login);
