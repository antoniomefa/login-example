const service = () => {
  return fetch(`https://jsonplaceholder.typicode.com/users`)
    .then(response => {
      return response.json();
    })
    .catch(err => {
      throw Error(err);
    });
};

export default service;
