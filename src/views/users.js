import React from "react";
import app from "../base";
import GetUsers from "../services/service";

import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { Button } from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";

class Usuarios extends React.Component {
  state = {
    usuarios: []
  };

  componentDidMount = () => {
    this.fetchData();
  };

  fetchData = () => {
    GetUsers()
      .then(data => {
        console.log("Leído del API");
        let fecha = new Date();
        localStorage.setItem("lastTime", fecha.toLocaleString());
        localStorage.setItem(
          "localUserData",
          JSON.stringify(data)
        ); /* Almacena en LocalStorage los datos de la consulta */
        this.setState({
          usuarios: data
        });
      })
      .catch(() => {
        console.log("Leído de LocalStorage");
        this.setState({
          usuarios: JSON.parse(
            localStorage.getItem("localUserData")
          ) /* Recupera la información almacenada de la última consulta */
        });
      });
  };

  render = () => (
    <Grid container direction="column" justify="center" alignItems="center">
      <Grid>
        <Typography variant="h3" className="Titulo">
          Listado de usuarios
        </Typography>
      </Grid>
      <Grid item>
        <Paper elevation={20}>
          <Grid container direction="row" justify="flex-end">
            <Button variant="contained" onClick={() => app.auth().signOut()}>
              Cerrar sesión
            </Button>
          </Grid>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell align="center">ID</TableCell>
                <TableCell align="left">NOMBRE</TableCell>
                <TableCell align="left">EMAIL</TableCell>
                <TableCell align="left">TELÉFONO</TableCell>
                <TableCell align="left">WEBSITE</TableCell>
                <TableCell align="left">EMPRESA</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.usuarios.map(row => (
                <TableRow key={row.id}>
                  <TableCell align="center">{row.id}</TableCell>
                  <TableCell align="left">{row.name}</TableCell>
                  <TableCell align="left">{row.email}</TableCell>
                  <TableCell align="left">{row.phone}</TableCell>
                  <TableCell align="left">{row.website}</TableCell>
                  <TableCell align="left">{row.company.name}</TableCell>
                </TableRow>
              ))}
            </TableBody>
            <caption>Basado en información del {localStorage.lastTime}</caption>
          </Table>
        </Paper>
      </Grid>
    </Grid>
  );
}

export default Usuarios;
