<h1>Login project<h1>

### `Instrucciones`

- Clonar el repositorio con el comando "git clone https://gitlab.com/antoniomefa/login-example.git"
- Ingresar al directorio del proyecto "cd login-example"
- Ejecutar el comando "yarn" para instalar las dependecias
- Ejecutar el comando "yarn start" para correr el proyecto en http://localhost:3000
- Ingresar con el usuario "usuario@email.com" y contraseña "123456"

### `Descripción`

- Aplicación en React.js
- Autenticación de usuarios con Firebase Authentication
- Obtención de usuarios mediante consulta de API REST hecha a "https://jsonplaceholder.typicode.com/users"
- Interface diseñada utilizando componentes de Material UI

Pantalla principal

<p align="center"><img src="public/assets/img/login.png"/></p>

Tabla de usuarios

<p align="center"><img src="public/assets/img/tabla.png"/></p>

Por: Antonio Mendiola Farías - 2020
