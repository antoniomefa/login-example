import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./views/login";
import Usuarios from "./views/users";
import NotFound from "./views/404";
import PrivateRoute from "./services/privateRoute";
import { AuthProvider } from "./services/auth";

import "./App.css";

function App() {
  return (
    <AuthProvider>
      <Router>
        <Switch>
          <PrivateRoute exact path="/" component={Usuarios} />
          <Route exact path="/login" component={Login} />;
          <Route component={NotFound} />;
        </Switch>
      </Router>
    </AuthProvider>
  );
}

export default App;
